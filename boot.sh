#!/bin/bash
#
# If cannot execute binary file exec format error is received 
# it is because the binary is prepared in an 64-bit machine
# and is tried to be run on an 32-bit machine

############################################################

# Try 'wget'ing google.com to ensure there is an active
# internet connection before running the whole script

isConnected=true
while [ $isConnected = true ]; do
	wget -q --spider http://google.com
	if [ $? -eq 0 ]; then
		isConnected=false
	else
		echo "No Connection"
	fi
	sleep 1
done

# Get the current users sudo pass from boxlist WS
# The username has to be in the database 
# If not sudo commands will not work (airmon-ng airodump-ng etc.)

username=$(logname)
boxlistWsUrl="http://azurewsv2.eventgates.com/il/boxlist"

boxlist=$(wget -O - -o /dev/null $boxlistWsUrl)
regex="\"pw\":\"[0-9A-Za-z]*\",\"un\":\"${username}\""
pair=$(echo $boxlist | egrep -o $regex)
password=$(echo $pair | cut -d'"' -f 4)

# Run sudo once to use the password
echo $password | sudo -S ifconfig wlan1 up
sleep 1

# Check the git repo if there are any updated files
# and replace the old ones with the new files
# This script assumes the response from 'git pull' command
# will return a string "Already-up-to-date." exactly. If
# this string changes the code will break.

# TODO : file updates should be moved to a server rather
# than relying on a git repo

# Git repo is in a directory called remote-machine-bundle 
# so the 'git pull' should be executed in that directory
cd $HOME/remote-machine-bundle
status=$(git pull)
if [ "$status" = "Already up-to-date." ]
then
  	echo "Scripts are up to date"
else
# All the updated filed are setup in this else branch

# Get the wlan interface name and also the network node name
# (i.e "uname -n) to replace defaults in the files below  
# TODO : wlanif may get more than on interface name
# fix this to get the interface that will be used with
# hostapd. Currently this is getting the second wlan interface

	# Run sudo once to use the password
	echo $password | sudo -S ifconfig
	sleep 1

	wlanif=$(ifconfig | egrep -o 'wlan([0-9])*') | head -n 2
	ssid=$(uname -n)

# Make sure the current directory is the git repo
	cd $HOME/remote-machine-bundle

# Try to replace the default values with box specific ones
# Warning: the -n option is for quiet execution meaning no 
# error or success messages will be printed
	sed -n -i s/HOME_DIR/$HOME/g calibration.conf
	sed -n -i s/wlaninterface/$wlanif/g dnsmasq.conf
	sed -n -i s/wlaninterface/$wlanif/g hostapd.conf
	sed -n -i s/password/$ssid/g hostapd.conf
	sed -n -i s/thisssid/$ssid/g hostapd.conf

# Until the next comment line (just before chmod,chown) , the 
# files are checked for existence and moved to their	
# relative directories 
	test -d $HOME/.config/autostart/ && mkdir $HOME/.config/autostart

	test -e dnsmasq.conf && sudo mv dnsmasq.conf /etc
	test -e hostapd.conf && sudo mv hostapd.conf /etc/hostapd
	test -e calibration.conf && sudo mv calibration.conf /etc/init

	test -e gnome-terminal.desktop && sudo mv gnome-terminal.desktop $HOME/.config/autostart

	test -e config.properties && mv config.properties $HOME
	test -e Configure.jar && mv Configure.jar $HOME
	test -e boot.sh && mv boot.sh $HOME
	test -e sniffer.jar && mv sniffer.jar $HOME
	test -e RMCalibrator.jar && mv RMCalibrator.jar $HOME
	test -e daemon.jar && mv daemon.jar $HOME

# Give appropriate permissions to startup file
# (i.e gnome-terminal.desktop) and make the current
# user the owner of the file (does this make sense?)
	sudo chmod 644 $HOME/.config/autostart/gnome-terminal.desktop
	sudo chown $(logname) $HOME/.config/autostart/gnome-terminal.desktop
fi

# If there is an interface for Access Point functionality on
# the device (named wlan1), assign ip for it, up the its
# interface, reset hostapd and dnsmasq before anything else
# so it won't cause connectivity related issues

ap_interface_name=$(ifconfig | grep -o 'wlan1')
if [ "$ap_interface_name" = "wlan1" ]
then
	
	mac=$(ifconfig wlan0 | grep -o ..:..:..:..:..:..)
	var="var boxMac='$mac';"
	sudo sed -i "1s/.*/$var/" /etc/chilli/www/script.js


        sudo killall dnsmasq
        sudo killall hostapd

        sudo ifconfig wlan1 192.168.10.1/24
        sleep 1
        sudo service dnsmasq restart
        sleep 1
	sudo service chilli start
	sudo sysctl net.ipv4.ip_forward=1
	sudo iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
	gnome-terminal -e "bash -c 'echo $password | sudo -S hostapd -t /etc/hostapd/hostapd.conf'"

fi


# Changes current directory to HOME since all the application
# files reside in HOME directory
cd $HOME

# Starts calibration service, note that this also registers
# the current local ip of the box to the DB
#sudo service calibration start

# sniffer.jar is the main sniffer application, uses 
# airomon-ng(TODO : check if it is so) and airodump-ng 
# to start sniffing. Daemon.jar acts as a server to run
# calibration (TODO : then why is there a calibration service?)

gnome-terminal -e "java -jar sniffer.jar $password"
gnome-terminal -e "java -jar daemon.jar"
